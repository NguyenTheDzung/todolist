// local storage
// function btnadd() {
//   var taskList = document.getElementById("list").value;
//   const taskListExit = localStorage.getItem("list");

//   if (taskList != "") {
//     const taskListArray =
//       taskListExit && taskListExit.length ? taskListExit.split(",") : [];
//     console.log(taskListArray.includes(taskList));
//     if (taskListArray.map((item) => item?.trim()).includes(taskList?.trim())) {
//       alert("Task list này đã tồn tại!");
//     } else {
//       console.log(taskList);
//       taskListArray.push(taskList);
//       localStorage.setItem(
//         "list",
//         taskListExit ? taskListExit + ", " + taskList : taskList
//       );
//       alert("Thêm task list thành công!");
//     }
//   } else {
//     alert("Xin hãy nhập task list của bạn!");
//   }
// }


  var taskListsApi = "http://localhost:3000/taskLists";

  function start() {
    getTasks(renderTasks);

    addForm();
  }

  start();

  //check
  var list = document.querySelector("ul");
  list.addEventListener(
    "click",
    function (ev) {
      if (ev.target.tagName === "H2") {
        ev.target.classList.toggle("checked");
      }
    },
    false
  );


  // var list = document.querySelector("ul");
  // list.addEventListener(
  //   "click",
  //   function (ev) {
  //     if (ev.target.tagName === "BUTTON") {
  //       ev.target.classList.toggle(deleteTask());
  //     }
  //   },
  //   false
  // );
  

  
 


var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++){
    close[i].onclick = function(){
        var div = this.parentElement;
        div.style.display ="none";
    }
}

  function getTasks(callback) {
    fetch(taskListsApi)
      .then(function (response) {
        return response.json();
      })
      .then(callback);
  }

  function createTasks(data, callback) {
    var options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      body: JSON.stringify(data),
    };
    fetch(taskListsApi, options)
      .then(function (response) {
        response.json();
      })
      .then(callback);
  }

  
 
  function deleteTask(id) {
    console.log(id)
    var s = confirm("Bạn có chắc muốn xoá task này?");
    if (s) {
      var options = {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      };
      fetch(taskListsApi + "/" + id, options)
        .then(function (response) {
          response.json();
        })
        .then(function () {
            getTasks(renderTasks);
        });
    }
  }


  function renderTasks(tasks) {
   
    var tasksList = document.querySelector("#task-list");
    var htmls = tasks.map(function (task) {
    
      return`
        <li>
          <h2><i class="fa fa-file"></i> ${task.name}
          <button class="close" type="button" onclick="deleteTask(${task.id})">Delete</button>
          </h2>
        </li>

      `
    });
    var btn = document.getElementById('btn');
    // btn.addEventListener('click', deleteTask(tasks.id));
    tasksList.innerHTML = htmls.join("");
  //   var del = document.getElementsByClassName('btn');
  //   debugger
    
  //   for (var i = 0; i < del.length; i++) {
  //     del[i].addEventListener('click', deleteTask, false);

  // }

  }

  function addForm() {
    var createBtn = document.querySelector("#create");
    createBtn.onclick = function () {
      var name = document.querySelector('input[name="list"]').value;

      var formData = {
        name: name,
      };

      if (name != "") {
        alert("Thêm task list thành công!");
        createTasks(formData);
      } else {
        alert("Xin hãy nhập task list của bạn!");
      }
    };
  }


  //update
  // function updateCourse(id, formData, callback) {
  //     var options = {
  //         method: 'PUT',
  //         headers: {
  //             'Content-Type': 'application/json'
  //         },
  //         body: JSON.stringify(formData)
  //     }
  //     fetch(coursesApi + "/" + id, options)
  //         .then(function(response) {
  //             return response.json()
  //         })
  //         .then(callback)
  // }

  // function handleUpdateCourse(id) {
  //     // lấy ra text của name , description ở id được ấn nút edit
  //     var name = document.querySelector(`.course-item-${id} h4`).textContent;
  //     var description = document.querySelector(`.course-item-${id} p`).textContent;
  //     //  lưu text đó vào 2 ô input
  //     var nameEdit = document.querySelector('input[name = "name"]').value = name;
  //     var descriptionEdit = document.querySelector('input[name = "description"]').value = description;

  //     console.log(nameEdit.trim())
  //     console.log(descriptionEdit.trim())

  //     // thay thế nút create thành nút edit
  //     var btnEdit = document.querySelector('#btn-create')
  //     btnEdit.setAttribute('id', "btn-edit")
  //     btnEdit.textContent = 'edit'

  //     //bắt sự kiên onclick vào nút edit (là nút thay thế nút create )
  //     btnEdit.onclick = function() {
  //         // lấy ra text đã thay đổi
  //         var nameEdit2 = document.querySelector('input[name = "name"]').value
  //         var descriptionEdit2 = document.querySelector('input[name = "description"]').value
  //             // tạo 1 object tên formData
  //         var formData = {
  //                 name: nameEdit2.trim(),
  //                 description: descriptionEdit2.trim(),
  //             }
  //             // ta gọi lại hàm update và truyền vào 2 đối số là id và formData
  //         console.log(formData)
  //         updateCourse(id, formData)

  //         // đổi lại nút
  //         btnEdit.setAttribute('id', "btn-create")
  //         btnEdit.textContent = 'create'

  //         // reset lại ô input
  //         nameEdit2.textContent = ''
  //         descriptionEdit2.textContent = ''
  //             // vì chorme mình auto refesh nên chưa test

  //     }
  // }

